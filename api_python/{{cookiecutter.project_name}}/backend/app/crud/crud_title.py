import re
import urllib.request

from app.core.log import logger


class SimpleWebCRUD:
    def fetch_title(self, url):
        logger.info(f"start fetching title of {url}")
        """Fetch the webpage and extract the title."""
        try:
            with urllib.request.urlopen(url) as response:
                html = response.read().decode("utf-8")
            title_match = re.search("<title>(.*?)</title>", html, re.IGNORECASE)
            return title_match.group(1) if title_match else "Title not found."
        except Exception as e:
            return f"Error fetching page title: {str(e)}"


web_crud = SimpleWebCRUD()
