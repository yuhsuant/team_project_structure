import os

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
LOG_PATH = os.path.join(PROJECT_ROOT, "data", "logs")
PROJECT_NAME = "DEMO"
