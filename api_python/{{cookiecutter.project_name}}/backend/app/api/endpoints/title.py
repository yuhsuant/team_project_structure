from app.crud.crud_title import web_crud
from fastapi import APIRouter, Depends, Query, Request, Response, status

router = APIRouter()


@router.get("/")
def langchain_search_vertex_response(
    request: Request,
    website: str = Query(
        default="https://admazes.com/",
        title="website",
        description="type your website",
    ),
):
    final_response = web_crud.fetch_title(website)
    return final_response
