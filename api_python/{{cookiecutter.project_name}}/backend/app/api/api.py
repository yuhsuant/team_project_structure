from app.api.endpoints import calculate, title
from fastapi import APIRouter

api_router = APIRouter()
api_router.include_router(
    title.router,
    prefix="/title",
    tags=["crud_demo"],
)
api_router.include_router(
    calculate.router,
    prefix="/calculate",
    tags=["math"],
)
