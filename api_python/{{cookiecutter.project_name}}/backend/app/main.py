from app.api.api import api_router
from app.core.log import logger
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware


def get_app() -> FastAPI:
    app = FastAPI(title="Demo project")
    # Add CORS middleware to allow requests from any origin
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],  # Allows all origins
        allow_credentials=True,
        allow_methods=["*"],  # Allows all methods
        allow_headers=["*"],  # Allows all headers
    )
    """
    @app.on_event("startup")
    async def startup_event():
    # recommend connection once object to be define here, define well the lifecycle
    """
    # Include your API router
    app.include_router(api_router, prefix="/api")
    return app


app = get_app()


if __name__ == "__main__":
    get_app()
