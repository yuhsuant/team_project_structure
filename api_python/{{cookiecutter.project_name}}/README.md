* Project: {{cookiecutter.project_name}}
* Author: {{cookiecutter.author}}

# Project description

This project is a demo project to show how to link all code together.

# Project Structure:

```
├── Dockerfile
├── README.md
├── backend
│   ├── app
│   │   ├── api
│   │   │   ├── api.py
│   │   │   └── endpoints
│   │   │       ├── calculate.py
│   │   │       └── title.py
│   │   ├── core
│   │   │   └── log.py
│   │   ├── crud
│   │   │   └── crud_title.py
│   │   ├── data
│   │   │   └── logs
│   │   ├── db
│   │   ├── main.py
│   │   ├── schemas
│   │   ├── setting.py
│   │   └── test
│   └── cred
├── poetry.lock
├── pyproject.toml
├── rebuild.sh
├── rebuild_image.sh
└── requirements.txt

```
-------


## Instruction to be removed for the projectes

### Folder description
1. pyproject.toml: package dependency management, with formating hooker definition
2. poetry.lock: auto-generated file from poetry to block the package version
3. rebuild.sh: (mac) it has command inside execute once can rebuild the docker container
4. rebuild_image.sh: (mac) it has command inside execute once can rebuild the image and docker container
5. requirements.txt: package to install beside poetry
6. .gitignore: defined file to be ignore by git upload
7. .pre-commit-config.yaml: pre-commit hooker defined, can add extra on your need
8. **backend/ :** volumn mounted to docker container for auto refresh purpose

### backend folder description
1. **app/ :** api developement code location
    - api/: api endpoints router
    - core/: project sharable code
    - crud/: crud related code
    - data/: Where you can store your data / intermidiate data, it is also recommended to include data cleaning within your code to clean the unused data. generally it can have different folders within such as logs / raw / intermediate etc...
    - db/: db connection / life cycle code
    - schemas/: schema related code
    - test/: unittesting code location
    - main.py: application execution file
    - setting.py: constant defined file
2. **cred/ :** credential json file store folder
3. .env: can define environment variables, such as credentials, add into docker container with docker run command

###  configer instruction
1. to generate your own file tree
    * `brew install tree`(if not installed)
    * `tree -a`
2. logging usage
    * in `app/settings.py` change PROJECT_NAME / LOG_PATH(if needed) to fit your usecase
    * in your module you need to import as below:
        ```
        from app.core.log import logger

        logger.debug("debug message")
        logger.info("info message")
        ```
3. .gitignore remove some if needed
4. before commit: pre-commit setup
    ```
    (pip install poetry)
    (pip install pre-commit)
    pre-commit install
    ```
5. demo run `chmod +x rebuild_image.sh; ./rebuild_image.sh`
6. check api swagger doc: `http://localhost:8000/docs`
