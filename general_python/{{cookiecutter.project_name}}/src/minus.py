from src.log import logger

def minus(a: int, b: int) -> int:
    """Subtracts the second integer from the first and returns the result. test"""
    logger.debug(f"perform {a} - {b} calculation")
    return a - b
