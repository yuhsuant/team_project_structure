from src.log import logger

def add(a: int, b: int) -> int:
    """Adds two integers and returns the result. test"""
    logger.debug(f"perform {a} + {b} calculation")
    return a + b
