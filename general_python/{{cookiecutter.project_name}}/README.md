* Project: {{cookiecutter.project_name}}
* Author: {{cookiecutter.author}}

# Project description

This project is a demo project to show how to link all code together.

# Project Structure:

```
├── .env
├── .gitignore
├── .pre-commit-config.yaml
├── README.md
├── data
│   └── logs
├── main.py
├── requirements.txt
├── pyproject.toml
├── settings.py
├── src
│   ├── __init__.py
│   ├── log.py
└── test
    └── __init__.py

```
-------


## Instruction to be removed for the projectes

### Folder description
* main.py: the execution file `python main.py {defined attributes}`
* settings.py: define project used constants
* .env: can define environment variables, such as credentials
* .pre-commit-config.yaml: pre-commit hooker defined, can add extra on your need
* .gitignore: defined file to be ignore by git upload
* requirements.txt: package management
* pyproject.toml: package dependency management, with formating hooker definition
* **src/ :** All the source code you call from main.py execution file, can be split to different folders depending on the complexity of the project. Using either class or function on your choice.
* **test/ :** All the unit test case can be defined in this folder
* **data/ :** Where you can store your data / intermidiate data, it is also recommended to include data cleaning within your code to clean the unused data. generally it can have different folders within such as logs / raw / intermediate etc...
* **notebooks/ :** (can be created) if you have testing / analysis notebooks to keep

###  configer instruction
1. to generate your own file tree
    * `brew install tree`(if not installed)
    * `tree -a`
2. Sample code description
    * execute `python main.py minus 3 1` or `python main.py add --a 3 --b 1`
3. logging usage
    * in `settings.py` change PROJECT_NAME / LOG_PATH(if needed) to fit your usecase
    * in your module you need to import as below:
        ```
        from src.log import logger

        logger.debug("debug message")
        logger.info("info message")
        ```
4. .gitignore remove some if needed
5. before commit: pre-commit setup
    ```
    (pip install poetry)
    (pip install pre-commit)
    pre-commit install
    ```
