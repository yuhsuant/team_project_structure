import fire
from src.add import add
from src.minus import minus
from src.log import logger


class Calculator:
    def add(self, a: int, b: int) -> int:
        result = add(a, b)
        logger.info(f"result = {result}")
    
    def minus(self, a: int, b: int) -> int:
        """Subtracts two numbers."""
        result = minus(a, b)
        logger.info(f"result = {result}")
if __name__ == "__main__":
    fire.Fire(Calculator)
