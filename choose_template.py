import subprocess
import sys
from pathlib import Path


def main():
    if len(sys.argv) < 2:
        print("Usage: python choose_template.py [api_python|general_python]")
        sys.exit(1)

    template_choice = sys.argv[1]
    choice = {"api_python", "general_python"}  # Use a set for faster lookups
    if template_choice not in choice:
        print(f"Invalid template choice. Please choose from {choice}.")
        sys.exit(1)

    # Resolve the full path to the template directory
    template_path = Path(__file__).resolve().parent / template_choice

    subprocess.run(["cookiecutter", str(template_path)], check=True)


if __name__ == "__main__":
    main()
