# Instruction

## create folder
1. `pip install cookiecutter`
2. clone the repository `git clone ...` (if already cloned, please check if the code is the latest `git pull`)
4. execute the project structure as your need `python team_project_structure/choose_template.py general_python`
5. answer your project name and owner and the folder would be created locally

## change content
1. remove the sample code use your own code.
2. `pip install poetry`
3. `pip install pre-commit`
4. cd to your project repository `pre-commit install` --> this would then check your code format before commiting to github

## setup configuration
1. (.gitignore remove some if needed)
2. change setting's PROJECT_NAME / LOG_PATH(if needed) to fit your usecase

for api
* add packages, `poetry add {packagename(==version)}`
* change Dockefile if needed
